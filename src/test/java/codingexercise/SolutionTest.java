package codingexercise;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class SolutionTest {
	
	@Test
	void NumberOfResultsShouldBeCorrect() {
		assertEquals(1, Solution.generateCombinations("11").size());
		assertEquals(9, Solution.generateCombinations("22").size());
	}
	
	@Test
	void ResultShouldContainExpectedWord() {
		String input = "8378"; //TEST
		List<String> result = Solution.generateCombinations(input);
		assertTrue(result.contains("TEST"));
	}
	
	@Test
	void givenALargeNumberThenItShouldWorkFine() {
		String input = "93553663";
		List<String> result = Solution.generateCombinations(input);
		assertTrue(result.size() > 0);
	}
}
